# Security Releases as a Quality Engineer

## General process overview

Quality engineer is contributing to two types of security releases, **critical** and **non-critical**.

Quality engineer works closely with Release Managers and Security Engineers to ensure that the provided security fixes are correctly implemented and that they do not cause any regressions in the functionality of the product.

Both types of security releases have few things in common. Quality engineer: 

1. gets assigned an issue from Release manager which signals that the QA process can start along with the release package information.
1. prepares the issue and adds required steps to execute the QA tasks
1. notifies the Security Engineer to start the validation
1. hands-off the release to the Security Engineer and Release Manager

## Critical
*Needs to be added.*

## Non-critical

1. Prepare the environment for testing the security fixes after getting notified from the Release Manager.
    1. In the issue received from the Release Manager, there is a section to find links to packages.
    1. Confirm if there are any back-ported versions that needs to be tested. Generally we limit the back-ports to a total of 3 releases (e.g. if the current release is 10.5, include 10.4 and 10.3).
    1. Log into [Google Cloud Console](https://console.cloud.google.com/) (access to this should have been granted during on-boarding), create a new `Ubuntu 16.04 LTS` (`n1-standard-1 (1 vCPU, 3.75 GB memory)`) VM for each version of GitLab. Be sure to add your public SSH key if you plan to SSH in the VM from your local machine in the next steps. For more information, check out the [documentation](https://docs.gitlab.com/ee/install/google_cloud_platform/index.html)
    1. Install the `.deb` package from the job artifact:
        1. To find the package, first find the pipeline for the tag in the [pipelines page]. For instance, for the `10.5.8+ee.0` Omnibus tag, the pipeline is [#81284].
        1. Then on the pipeline page, click the `Ubuntu-16.04-staging` job in the `Staging_upload` stage. For instance, for the `10.5.8+ee.0` Omnibus tag, the job is [#2379548].
        1. Browse the job's artifacts, and find the `.deb` package under `pkg/ubuntu-xenial/`. For instance, for the `10.5.8+ee.0` Omnibus tag the package page is [https://dev.gitlab.org/gitlab/omnibus-gitlab/-/jobs/2379548/artifacts/file/pkg/ubuntu-xenial/gitlab-ee_10.5.8-ee.0_amd64.deb][package-page].
        1. Copy the `Download` link location. For instance, for the `10.5.8+ee.0` Omnibus tag the package link location is [https://dev.gitlab.org/gitlab/omnibus-gitlab/-/jobs/2379548/artifacts/raw/pkg/ubuntu-xenial/gitlab-ee_10.5.8-ee.0_amd64.deb][package-link-location].
        1. SSH into the VM you've setup in GCP (you can do it via the GCP console page).
        1. Create a `install-gitlab.sh` script in your home folder:

            ```
TEMP_DEB="$(mktemp)" &&
curl -H 'PRIVATE-TOKEN: <TOKEN>' '<PACKAGE_LINK_LOCATION>' -o "$TEMP_DEB" &&
sudo dpkg -i "$TEMP_DEB"
rm -f "$TEMP_DEB"
            ```
            Replace `<TOKEN>` with a `.org` personal access token so that the script can download the package, and replace `<PACKAGE_LINK_LOCATION>` with the package link location from the job's artifacts.
        1. Change the script's permission with `chmod +x install-gitlab.sh`.
        1. Run the script with `./install-gitlab.sh`.
        1. Once GitLab installed, set the `external_url` in `/etc/gitlab/gitlab.rb` with `vim /etc/gitlab/gitlab.rb`. You can find the VM's IP in the GCP console.
        1. Restart GitLab with `sudo gitlab-ctl restart`.
        1. Visit http://IP_OF_THE_GCP_VM and change `root`'s password.
    1. Once the environments are ready, capture the information to add to the QA issue.
1. Create the QA task issues for the release
    1. Create a QA task using the [QA Task Template](https://gitlab.com/gitlab-org/release/tasks/blob/master/.gitlab/issue_templates/QA-task.md) and cross-link the QA task issue to the security release issue.
    1. Name the issue such that we know it is a Security QA task, for example: `2018-04-10: Security 10.#.#.rc# QA Task`
    1. Fill in the information for the QA task issue. This includes the security fixes that has be be verified. See an example of this [here](https://gitlab.com/gitlab-org/release/tasks/issues/145)
    1. Fill in back-ported versions
1. Coordinate the validation of the release
    1. Current version
        1. Start the automated test run for GitLab QA:

            ```
GITLAB_USERNAME=root GITLAB_PASSWORD=<ROOT_PASSWORD> gitlab-qa Test::Instance::Any EE <VERSION> http://IP_OF_THE_GCP_VM
            ```
            Replace `<ROOT_PASSWORD>` with the password you've set for the `root` user, and replace `<VERSION>` with the actual package version. For instance, for the `10.5.8+ee.0` Omnibus tag the GitLab version is `10.5.8-ee`.
          * Triage the test result and validate that existing automated test suites pass with no regressed functionality 
          * TBD - `fill in more details when going through a security release`
        1. Notify the Security Engineer to verify the security fixes for the release. 
        * The manner in which the security fixes are verified can be done in two ways.
            1. By the Quality Engineer executing the validation with close collaboration and guidance from the Security Engineer 
            1. By the Security Engineer executing the validation with the Quality Engineer monitoring the steps.
        * *Note*: When encountered with deadline and resource constraints, the work should be assigned for efficiency. Security Engineer should own verifying complex security validations while Quality Engineer is encouraged to help out with simpler validations. However it is important that the Security team signs off on the result of the validation.
    1. Back-ported versions    
      1. Start the automated test run for GitLab QA on back-ported environments (similar command as for the "Current version")
          * Triage the test result and validate that existing automated test suites pass with no regressed functionality 
          * TBD - `fill in more details when going through a security release`
    1. Ensure that all the items for validation are validated and checked off before moving forward
1. Hand off the release assignment
   1. Once all the validation is completed, Quality Engineer un-assigns him/herself from the release issue leaving only the Security Engineer and the Release Manager.

[pipelines page]: https://dev.gitlab.org/gitlab/omnibus-gitlab/pipelines?scope=tags
[#81284]: https://dev.gitlab.org/gitlab/omnibus-gitlab/pipelines/81284
[#2379548]: https://dev.gitlab.org/gitlab/omnibus-gitlab/-/jobs/2379548
[package-page]: https://dev.gitlab.org/gitlab/omnibus-gitlab/-/jobs/2379548/artifacts/file/pkg/ubuntu-xenial/gitlab-ee_10.5.8-ee.0_amd64.deb
[package-link-location]: https://dev.gitlab.org/gitlab/omnibus-gitlab/-/jobs/2379548/artifacts/raw/pkg/ubuntu-xenial/gitlab-ee_10.5.8-ee.0_amd64.deb

[Return to Security Guide](../security.md)