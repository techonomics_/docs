Also see [the GitLab Handbook on the Critical Release Process](https://about.gitlab.com/handbook/engineering/critical-release-process/)
for the timeline of handling a critical security release.
Only a subset of the security releases are critical.

# Security Releases

Security vulnerabilities in GitLab and its dependencies are to be addressed with
the highest priority.

Security releases are naturally very similar to [patch releases](patch.md), but
on a much shorter timeline. The goal is to make a security release available as
soon as possible, while ensuring that the security issue is properly addressed
and that the fix does not introduce regressions.

Depending on the severity and the attack surface of the vulnerability, an
immediate patch release consisting of just the security fix may be warranted.
For less severe issues, it may be acceptable to include the fix in a future
patch. This is one case where the release manager _does not_ have final say
concerning a release, and he or she should consult with the GitLab development
team as well as any applicable security experts, such as the person disclosing
the issue.

As noted in the [checklist](https://about.gitlab.com/handbook/engineering/critical-release-process/),
a specific "security release manager" is designated to act as the release
manager for security releases. By default, the security release manager is the
RM from the _previous_ release. Having one RM for the security release - even
though a security release will typically span multiple prior versions - is more
efficient and less likely to lead to confusion or unintended "leaking" of the
vulnerability. By designating the RM from the _previous_ release, the RM for the
_current_ release is not hindered in their work to get out the next release.

## Guides by role

- [Developer](security/developer.md)
- [Release Manager](security/release-manager.md)

---

[Return to Guides](../README.md)
